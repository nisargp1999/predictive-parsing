// Predictive_Parsing.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stack>
#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <iomanip>
using namespace std;

void printTable(stack<string> matched, stack<string> myStack, stack<string> input, string action, int& temp);

int main()
{
	ofstream output;
	output.open("output.txt");

	int temp = 0;

	string tempString = "";
	string singString = "";
	string action = "";

	map<string, string> E;		//Non terminal E
	map<string, string> e;		//Non terminal E'
	map<string, string> T;		//Non terminal T
	map<string, string> t;		//Non terminal T'
	map<string, string> F;		//Non terminal F

	E["i"] = "TE\'";
	E["+"] = "";
	E["*"] = "";
	E["("] = "TE\'";
	E[")"] = "";
	E["$"] = "";

	e["i"] = "";
	e["+"] = "+TE\'";
	e["*"] = "";
	e["("] = "";
	e[")"] = "ε";
	e["$"] = "ε";

	T["i"] = "FT\'";
	T["+"] = "";
	T["*"] = "";
	T["("] = "FT\'";
	T[")"] = "";
	T["$"] = "";

	t["i"] = "";
	t["+"] = "ε";
	t["*"] = "*FT\'";
	t["("] = "";
	t[")"] = "ε";
	t["$"] = "ε";

	F["i"] = "i";
	F["+"] = "";
	F["*"] = "";
	F["("] = "(E)";
	F[")"] = "";
	F["$"] = "";

	

	stack <string> matched;
	stack <string> myStack;
	stack <string> input;

	matched.push(" ");
	myStack.push("$");
	myStack.push("E");
	input.push("$");
	input.push("i");
	input.push("*");
	input.push("i");
	input.push("+");
	input.push("i");
	
	cout << "Matched\tStack\tInput\tAction" << endl;
	printTable(matched, myStack, input, action, temp);
	while (myStack.top() != "$"/* || input.top() != "$"*/)
	{
		if (myStack.top() == input.top())
		{
			action = "match " + input.top();
			if (matched.top() == " ")
			{
				matched.pop();
			}
			matched.push(input.top());
			input.pop();
			myStack.pop();
		}
		else
		{
			if (myStack.top() == "E")
			{
				myStack.pop();
				if (myStack.top() == "\'")		//If E'
				{
					myStack.pop();
					tempString = e[input.top()];
					if (tempString != "ε")
					{
						action = "output E\' -> " + tempString;
						for (int i = tempString.length() - 1; i >= 0; i--)
						{
							singString = tempString.substr(i, 1);
							myStack.push(singString);

						}
					}
					else {
						action = "output E\' -> ε";
					}
				}
				else {							//If E
					string temp1 = input.top();
					tempString = E[temp1];
					action = "output E\' -> " + tempString;
					for (int i = tempString.length() - 1; i >= 0; i--)
					{
						singString = tempString.substr(i, 1);
						myStack.push(singString);
					}
				}
			}
			else if (myStack.top() == "T")
			{
				myStack.pop();
				if (myStack.top() == "\'")		//If T'
				{
					myStack.pop();
					tempString = t[input.top()];
					if (tempString != "ε")
					{
						action = "output E\' -> " + tempString;
						for (int i = tempString.length() - 1; i >= 0; i--)
						{
							singString = tempString.substr(i, 1);
							myStack.push(singString);

						}
					}
					else {
						action = "output T\' -> ε";
					}
				}
				else {							//If T
					string temp1 = input.top();
					tempString = T[temp1];
					action = "output E\' -> " + tempString;
					for (int i = tempString.length() - 1; i >= 0; i--)
					{
						singString = tempString.substr(i, 1);
						myStack.push(singString);
					}
				}
			}
			else if (myStack.top() == "F")		//If F
			{
				myStack.pop();
				string temp1 = input.top();
				tempString = F[temp1];
				action = "output E\' -> " + tempString;
				for (int i = tempString.length() - 1; i >= 0; i--)
				{
					singString = tempString.substr(i, 1);
					myStack.push(singString);
				}
			}
			
		}
		printTable(matched, myStack, input, action, temp);
	}
	
    return 0;
}



void printTable(stack<string> matched, stack<string> myStack, stack<string> input, string action, int& temp)
{

	ofstream output;
	output.open("output.txt", ofstream::out | ofstream::app);
	if (temp == 0)
	{
		output << "Matched\tStack\tInput\tAction" << endl;
		temp = 1;
	}
	vector<string> tempMatched;
	while (!matched.empty())
	{
		tempMatched.push_back(matched.top());
		matched.pop();
	}
	reverse(tempMatched.begin(), tempMatched.end());
	for (int i = 0; i < tempMatched.size(); i++)
	{
		cout << tempMatched[i];
		output << tempMatched[i];
	}
	cout << "\t";
	output << "\t";
	while (!myStack.empty())
	{
		cout << myStack.top();
		output << myStack.top();
		myStack.pop();
	}
	cout << "\t";
	output << "\t";
	while (!input.empty())
	{
		cout << input.top();
		output << input.top();
		input.pop();
	}
	cout << "\t" << action << endl;
	output << "\t" << action << endl;

}